#### REQUIREMENTS
* Virtual enviroment (Python 3), we recommend to use as default name: env 
* Install requirements.txt
* MEDNLP project (scripts will use it)

#### EXECUTION
* ```source env/bin/activate```
* ```jupyter notebook```

#### DATA MANAGEMENT
* Notebooks
    * document_selection: contains the scripts to get the documents to evaluate. We create one per study
    * inception: contains the scripts for evaluation tool creation. We create one per study-site

* Data: this folder must not be in the repository, the structure is the same like the Notebooks.

#### Owners of the repo:
* Daniel Marfetán
* Raúl Arnaiz

